patient_creation_request_schema = {
    'type':       'object',
    'properties': {
        'patient_info':{
            'type':       'object',
            'properties': {
                'full_name':   {'type': 'string'},
                'email':       {'type': 'string'},
                'cpf':         {'type': 'string'},
                'rg':          {'type': 'string'},
                'phone':       {'type': 'string'},
                'phone2':      {'type': 'string'},
                'following':   {'type': 'string'},
                'medicaments': {'type': 'string'},
            },
            'required': ['full_name', 'email', 'cpf', 'rg', 'phone']
        },
        'patient_responsible':{
            'type':'array',
            'items': {
                'type': 'object',
                'properties':{
                    'full_name':   {'type': 'string'},
                    'cpf':         {'type': 'string'},
                    'rg':          {'type': 'string'},
                    'phone':       {'type': 'string'},
                    'phone2':      {'type': 'string'},
                    'email':      {'type': 'string'},
                },
                'required': ['full_name', 'email', 'cpf', 'rg', 'phone']
            },
        },
    },
    'required':['patient_info']
}
from ..common.models import Timestamped
from mongoengine import *
import datetime

class PatientResponsible(EmbeddedDocument):
    full_name = StringField()
    email = StringField()
    cpf = StringField()
    rg = StringField()
    address = StringField()
    phone = StringField()
    phone2 = StringField()

    @property
    def as_dict(self):
        return {
            'full_name': self.full_name,
            'email': self.email,
            'cpf': self.cpf,
            'rg': self.rg,
            'address': self.address,
            'phone': self.phone,
            'phone2': self.phone2
        }

class Patient(Timestamped):

    full_name = StringField()
    email = StringField()
    cpf = StringField(unique=True)
    rg = StringField(unique=True)
    address = StringField()
    phone = StringField()
    phone2 = StringField()
    following = StringField()
    medicaments = StringField()
    responsible = EmbeddedDocumentListField(PatientResponsible)
    owner = UUIDField()
    patient_uuid = UUIDField()

    @property
    def as_dict(self):
        return {
            'full_name': self.full_name,
            'email': self.email,
            'cpf': self.cpf,
            'rg': self.rg,
            'address': self.address,
            'phone': self.phone,
            'phone2': self.phone2,
            'following': self.following,
            'medicaments': self.medicaments,
            'responsible': [responsible.as_dict for responsible in self.responsible],
            'owner': str(self.owner),
            'patient_uuid': str(self.patient_uuid),
            'pk': str(self.pk),
        }

class Consult(Timestamped):

    time = DateTimeField()
    id_agenda = ObjectIdField()
    uuid_patient = UUIDField()
    patient_name = StringField()
    note_raw = StringField()
    remainder = StringField()
    owner = UUIDField()

    @property
    def as_dict(self):
        return {
            "time": self.time.strftime("%d/%m/%YT%H:%M:%S"),
            "patient_name": self.patient_name,
            "note_raw": self.note_raw,
            "remainder": self.remainder,
            'pk': str(self.pk),
        }
import falcon
import uuid
import datetime
from falconjsonio.schema import request_schema
from .jsonSchemas import * 
from mongoengine.errors import NotUniqueError, DoesNotExist
from .models import Patient, PatientResponsible, Consult

class PatientCRUD(object):
    def on_get(self, req, resp, pk):
        try:
            patient = Patient.objects.get(pk=pk)
            resp.media = patient.as_dict
        
        # Erro caso a pk solicitada não exista
        except DoesNotExist:
            resp.media = {
                "message": "O usuário solicitado não exste."
            }

    @request_schema(patient_creation_request_schema)
    def on_post(self, req, resp):
        try:
            # Cria o paciente.
            patient = Patient(**req.context['doc']['patient_info'])
            patient.owner=req.context['user'].uuid
            patient.patient_uuid = uuid.uuid1()
            
            # Adiciona os responsáveis pelo paciente.
            if 'patient_responsible' in req.context['doc']:
                for respo in req.context['doc']['patient_responsible']:
                    patient.responsible.append(PatientResponsible(**respo))

            patient.save()

            resp.media = patient.as_dict

        except NotUniqueError: 
            resp.media = {
                "message": "Um paciente com este CPF ou RG já existe"
            }

    def on_patch(self, req, resp, pk):
        try:
            # Pega o paciente no banco.
            patient = Patient.objects.get(pk=pk)
            
            # Atualiza as infos do paciente.
            for key, value in req.context['doc']['patient_info'].items():
                setattr(patient, key, value)

            # Atualiza as infos do responsável pelo paciente.
            if "patient_responsible" in req.context['doc']:
                for idx, responsible in enumerate(req.context['doc']['patient_responsible']):
                    for key, value in responsible.items():
                        setattr(patient.responsible[idx], key, value)
            
            # Adiciona um ou mais responsáveis.
            if "responsible_add" in req.context['doc']:
                for responsible in req.context['doc']['responsible_add']:
                    patient.responsible.append(PatientResponsible(**responsible))

            patient.save()
        except DoesNotExist:
            resp.media = {
                "message": "O usuário solicitado não exste."
            }

    def on_delete(self, req, resp, pk):
        try:
            Patient.objects.get(pk=pk).delete()

            resp.media = {
                "message": "Paciente deletado."
            }
        except DoesNotExist:
            resp.media = {
                "message": "O usuário que você está tentando deletar não exste."
            }

class PatientList(object):
    def on_get(self, req, resp):
        action = req.context['doc']['action']
        user = req.context['user']

        if action == 'paginate':
            offset = req.context['doc']['offset']
            limit = req.context['doc']['limit']
            queryset = Patient.objects(owner=user.uuid).skip(offset).limit(limit)
            body = {
                "action": req.context['doc']['action'],
                "result": [item.as_dict for item in queryset]
            }
            resp.media = body
        
        if action == 'names':
            queryset = Patient.objects(owner=user.uuid).only('pk', 'full_name')
            body = {
                "action": req.context['doc']['action'],
                "result": [{"full_name":item.full_name, "pk":str(item.pk)} for item in queryset]
            }
            resp.media = body
        
        if action == 'date':
            pass

class ConsultCRUD(object):
    def on_get(self, req, resp, pk):
        try:
            consult = Consult.objects.get(pk=pk)
            resp.media = consult.as_dict
        except DoesNotExist:
            resp.media = {
                "message": "A consulta que você está buscando não exste."
            }

    def on_post(self, req, resp):
        consult = Consult()
        consult.patient_name = req.context['doc']['patient_name']
        consult.patient_uuid = req.context['doc']['patient_uuid']
        consult.time = datetime.datetime.strptime(req.context['doc']['time'], "%d/%m/%YT%H:%M:%S")
        consult.owner = req.context['user'].uuid

        body = {
            "message": "Consulta criada.",
            "consult": consult.as_dict
        }

        resp.media = body

    def on_patch(self, req, resp, pk):
        pass
    
    def on_delete(self, req, resp, pk):
        try:
            Consult.objects.get(pk=pk).delete()

            resp.media = {
                "message": "Consulta deletada."
            }
        except DoesNotExist:
            resp.media = {
                "message": "A consulta que você está tentando deletar não exste."
            }

from mongoengine import *

class Timestamped(Document):
    meta = {
        'abstract': True,
    }
    created_at = DateTimeField()
    updated_at = DateTimeField()
    
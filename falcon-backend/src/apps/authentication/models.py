import bcrypt
import uuid
import datetime
from ..common.models import Timestamped
from mongoengine import *

class User(Timestamped):

    plan_types = ['free', 'trimestral', 'anual']
    acc_types = ['starter']

    name = StringField()
    last_name = StringField()
    password = BinaryField()
    email = StringField(unique=True)
    plan = StringField() # gratis, trimestral, semestral, anual 
    telephone = StringField()
    crp = StringField()
    cpf = StringField()
    uuid = UUIDField()
    status = StringField() # active, retry, pendente
    acc_type = StringField()
    plan_type = StringField()

    @property
    def as_dict(self):
        return {
            'email': self.email,
            'name': self.name,
            'last_name': self.last_name,
            'plan': self.plan,
            'telephone': self.telephone,
            'crp': self.crp,
            'cpf': self.cpf,
            'status': self.status,
            'acc_type': self.acc_type,
            'plan_type': self.plan_type,
        }

    def set_password(self, raw_password):
        pw = bytes(raw_password, 'utf-8')
        self.password = bcrypt.hashpw(pw, bcrypt.gensalt())
    
    def check_password(self, raw_password):
        pw = bytes(raw_password, 'utf-8')
        if bcrypt.checkpw(pw, self.password):
            return True

        return False
    
    @classmethod
    def create_user(cls, **kw):
        user = User(name=kw['name'], last_name=kw['last_name'],email=kw['email'])
        user.acc_type = "starter"
        user.plan_type = "free"
        user.uuid = uuid.uuid1()
        user.set_password(kw['password'])
        user.save()

        return user

from falcon_auth import JWTAuthBackend
from .models import User

def user_loader(payload): 
    user = User.objects.get(email=payload['user']['email'])
    
    if user.check_password(payload['user']['password']):
        return user
    else:
        return None

auth_backend = JWTAuthBackend(user_loader, "your-256-bit-secret", verify_claims=['exp'], required_claims=['exp'])

import falcon
from .models import User
from falcon_auth import JWTAuthBackend
from .jsonSchemas import *
from falconjsonio.schema import request_schema

class UserResource(object):

    auth = {
        'exempt_methods': ['POST']
    }

    def on_get(self, req, resp):
        body = {
        "code": 200,
        "user": req.context['user'].as_dict,
        }

        resp.media = body

    @request_schema(user_creation_request_schema)
    def on_post(self, req, resp):
        user = User.create_user(**req.context['doc'])

        body = {
            'code': '200',
            'user': user.as_dict
        }

        resp.media = body


auth_backend = JWTAuthBackend(None, "your-256-bit-secret", verify_claims=['exp'], required_claims=['exp'])

class GetToken():

    _getToken = auth_backend.get_auth_token

    @request_schema(gettoken_request_schema)
    def on_post(self, req, resp):
        token = self._getToken({
            "email": req.context['doc']['email'],
            "password": req.context['doc']['password']
            })

        body = {
            "code": 200,
            "token": token,
        }

        resp.media = body
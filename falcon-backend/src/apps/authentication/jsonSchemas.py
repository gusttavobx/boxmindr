user_creation_request_schema = {
    'type':       'object',
    'properties': {
        'name':         {'type': 'string'},
        'last_name':    {'type': 'string'},
        'email':        {'type': 'string'},
        'password':     {'type': 'string'},
    },
    'required': ['name', 'last_name', 'email', 'password'],
}
gettoken_request_schema = {
    'type':       'object',
    'properties': {
        'email':         {'type': 'string'},
        'password':   {'type': 'string'}
    },
    'required': ['email', 'password'],
}
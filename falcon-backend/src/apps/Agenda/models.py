import datetime
from ..common.models import Timestamped
from mongoengine import *

class Agenda(Timestamped):

    time = DateTimeField()
    patient_name = StringField()
    owner = UUIDField()
    patient_uuid = UUIDField()
    duration = IntField()
    miss = BooleanField()

    @property
    def as_dict(self):
        return {
            "time": self.time.strftime("%d/%m/%YT%H:%M:%S"),
            "patient_name": self.patient_name,
            "patient_uuid": str(self.patient_uuid),
            "duration": str(self.duration),
            "miss": str(self.miss)
        }
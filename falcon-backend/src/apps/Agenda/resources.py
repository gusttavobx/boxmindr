import uuid
from .models import Agenda
from mongoengine.errors import DoesNotExist
import datetime

class AgendaCRUD(object):
    def on_get(self, req, resp, pk):
        try:
            agenda = Agenda.objects.get(pk=pk)
            resp.media = agenda.as_dict
        
        # Erro caso a pk solicitada não exista
        except DoesNotExist:
            resp.media = {
                "message": "O horario solicitado não exste."
            }

    def on_post(self, req, resp):
        agenda = Agenda()
        agenda.time = datetime.datetime.strptime(req.context['doc']['time'],"%d/%m/%YT%H:%M:%S")
        agenda.owner = req.context['user'].uuid
        agenda.save()

        resp.media = agenda.as_dict

    def on_patch(self, req, resp, pk):
        try:
            agenda = Agenda.objects.get(pk=pk)

            for key, value in req.context['doc'].items():
                setattr(agenda, key, value)
            
            agenda.save()

            resp.media = {
                'code': 200,
                'agenda': agenda.as_dict
            }

        except DoesNotExist:
            resp.media = {
                "message": "O horario solicitado não exste."
            }

    def on_delete(self, req, resp, pk):
        try:
            Agenda.objects.get(pk=pk).delete()

            resp.media = {
                "message": "Horario deletado."
            }
        except DoesNotExist:
            resp.media = {
                "message": "O horario que você está tentando deletar não exste."
            }

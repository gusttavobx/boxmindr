import falcon
import json
from mongoengine import *
from apps.authentication import resources as auth_resources
from apps.agenda import resources as agenda_resources
from apps.therapy import resources as therapy_resources
from apps.authentication.middleware import auth_backend 
from falcon_auth import FalconAuthMiddleware
from falconjsonio.middleware import *

connect('tumblelog')

class Something():
    
    def on_get(self, req, resp, name):

        body = {
        "code": 200,
        "message": "This is just a message",
        "user": str(req.context['user'].id),
        'name': name
        }

        resp.body = json.dumps(body)
    

class Something2():
    
    def on_get(self, req, resp):

        body = {
        "code": 200,
        "message": "This is just a message",
        "user": req.context['user'].as_dict,
        "no-name":''
        }

        resp.body = json.dumps(body)

auth_middleware = FalconAuthMiddleware(auth_backend, exempt_routes=['/gettoken'], exempt_methods=['HEAD', 'OPTIONS'])

api = application = falcon.API(middleware=[
    auth_middleware,
    RequireJSON(),
    JSONTranslator(),
    ])

api.add_route('/{name}', Something())
api.add_route('/gettoken', auth_resources.GetToken())
api.add_route('/userresource', auth_resources.UserResource())
api.add_route('/patientcrud', therapy_resources.PatientCRUD())
api.add_route('/patientcrud/{pk}', therapy_resources.PatientCRUD())
api.add_route('/consultcrud', therapy_resources.ConsultCRUD())
api.add_route('/consultcrud/{pk}', therapy_resources.ConsultCRUD())
api.add_route('/agendacrud', agenda_resources.AgendaCRUD())
api.add_route('/agendacrud/{pk}', agenda_resources.AgendaCRUD())

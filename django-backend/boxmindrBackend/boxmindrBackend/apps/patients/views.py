import datetime
from django.utils import timezone
from rest_framework_mongoengine import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.filters import SearchFilter
from rest_framework.pagination import PageNumberPagination
from mongoengine.queryset.visitor import Q


from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from Calendar.models import Schedules

from Authentication.models import UserProfile
from . import serializers
from . import models

UUID="1fa39c9a-c4f6-11e8-b8b2-bcee7b97176b"

def view_timer(func):
    def timer(self, request, *args, **kwargs):
        start = timezone.now()
        res = func(self, request, *args, **kwargs)
        end = timezone.now()
        print('Timer: {0}'.format(end-start))
        return res
    return timer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 100



class PatientsListViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    # authentication_classes = (SessionAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.PatientsSerializer
    # pagination_class = StandardResultsSetPagination

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PatientsListViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        
        # psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=UUID)

    def get_kwargs_for_filtering(self):
        filtering_kwargs = {} 
        for field in self.my_filter_fields: # iterate over the filter fields
            field_value = self.request.query_params.get(field) # get the value of a field from request query parameter
            if field_value: 
                filtering_kwargs[field] = field_value
        return filtering_kwargs 


    def get_queryset(self):

        # psycologist = UserProfile.objects.get(
            # email=self.request.user)
        queryset = models.Patients.objects(psycologist_uuid=UUID)
        if self.request.query_params.get('search'):
            value = self.request.query_params.get('search')
            f, *args = value.split(" ")
            print(f)
            print(args[0])
            if not len(args):
                queryset = queryset.filter(first_name__icontains=value)
            else:
                queryset = queryset.filter(first_name__icontains=f,
                last_name__icontains=args[0])

        return queryset

    # def get_filter_kwargs(self):
    #     filter_kwargs={}
    #     for field in self.search_fields:



class PackagesListViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.PackageSerializer

    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=UUID)

    def get_queryset(self):

        psycologist = UserProfile.objects.get(
            email=self.request.user)
        queryset = models.Package.objects(psycologist_uuid=UUID)
        return queryset



class ConsultsListViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    # authentication_classes = (SessionAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.ConsultsSerializer


    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConsultsListViewSet, self).dispatch(request, *args, **kwargs)


    def perform_create(self, serializer):
        
        # psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=UUID)

    def get_queryset(self):

        # psycologist = UserProfile.objects.get(
        #     email=self.request.user)
        queryset = models.Consults.objects(psycologist_uuid=UUID)
        return queryset


@api_view(["GET"])
def NextConsultsPatients(request, *args, **kwargs):
    queryset=Schedules.objects(consult_time__gt=datetime.datetime.now()).distinct(field="patient")
    
    print("queryset: ",queryset)
    for item in queryset:
        if len(item.next_consult) > 0:
            item.next=Schedules.objects(id__in=item.next_consult).order_by("-consult_time")
    
    serializer=serializers.PatientsPlusSchedules(queryset, many=True)
    return Response(serializer.data)



@api_view(["GET"])
def PatientsToday(request, *args, **kwargs):
        now=datetime.datetime.now()
        day_end=datetime.datetime(now.year,now.month,now.day,23,59,59)
        queryset=Schedules.objects(consult_time__gt=now,
                                consult_time__lt=day_end).distinct(field="patient")
        

        for item in queryset:
            if len(item.next_consult) > 0:
                item.next=Schedules.objects(id__in=item.next_consult).order_by("consult_time")
                print("item.next: ",item.next)
        
        serializer=serializers.PatientsPlusSchedules(queryset, many=True)
        return Response(serializer.data)





@api_view(["GET"])
def PatientsPerRange(request, *args, **kwargs):
    print("request.query_params: ",request.query_params)
    if 'range' in request.query_params:
        if request.query_params['range'] == "today":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            day_end=datetime.datetime(now.year,now.month,now.day,23,59,59)
            queryset=Schedules.objects(Q(consult_time__gt=today) &
                                Q(consult_time__lt=day_end)).distinct(field="patient")
            

            for item in queryset:
                if len(item.next_consult) > 0:
                    item.next=Schedules.objects(id__in=item.next_consult).order_by("-consult_time")
                    print("item.next: ",item.next)
            
            serializer=serializers.PatientsPlusSchedules(queryset, many=True)
            return Response(serializer.data)

        if request.query_params['range'] == "week":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            difference1 = datetime.timedelta(weeks=1)
            week_end=today+difference1
            queryset=Schedules.objects(Q(consult_time__gt=today) &
                                Q(consult_time__lt=week_end)).distinct(field="patient")
            

            for item in queryset:
                if len(item.next_consult) > 0:
                    item.next=Schedules.objects(id__in=item.next_consult).order_by("-consult_time")
                    print("item.next: ",item.next)
            
            serializer=serializers.PatientsPlusSchedules(queryset, many=True)
            return Response(serializer.data)

        if request.query_params['range'] == "month":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            difference1 = datetime.timedelta(weeks=4)
            month_end=today+difference1
            queryset=Schedules.objects(Q(consult_time__gt=today) &
                                Q(consult_time__lt=month_end)).distinct(field="patient")
            

            for item in queryset:
                if len(item.next_consult) > 0:
                    item.next=Schedules.objects(id__in=item.next_consult).order_by("-consult_time")
                    print("item.next: ",item.next)
            
            serializer=serializers.PatientsPlusSchedules(queryset, many=True)
            return Response(serializer.data)
    else: 
        return Response(None)



@api_view(["GET"])
def ConsultsPerPatient(request, *args, **kwargs):
    if request.query_params["id"]:
        patient_id=request.query_params["id"]
        print("patient_id: ",patient_id)
        queryset=models.Consults.objects(patient=patient_id)
        serializer=serializers.ConsultsSerializer(queryset, many=True)
        return Response(serializer.data)
    else:
        pass
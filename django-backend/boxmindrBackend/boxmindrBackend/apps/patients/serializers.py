from datetime import datetime
from rest_framework_mongoengine import serializers
from rest_framework import serializers as drf_serializer
from django.utils import timezone
from Calendar.serializers import SchedulesSerializer
from Calendar.models import Schedules
from . import models

class PatientsSerializer(serializers.DocumentSerializer):


    class Meta:
        model = models.Patients
        fields="__all__"
        extra_kwargs={
            'payment_type': {'read_only': True},
            'psycologist_uuid': {'read_only': True},
            'patient_responsible': {'read_only': True},
            'created_at': {'read_only': True},
            'next_consult': {'read_only': True},
        }
        
    
    
    def create(self, validated_data):

        patient = models.Patients.objects.create(**validated_data,created_at=timezone.now())
        return patient

    def update(self, instance, validated_data):

        instance.email = validated_data['email']
        instance.first_name = validated_data['first_name']
        instance.last_name=validated_data['last_name']
        instance.cpf=validated_data['cpf']
        instance.rg=validated_data['rg']
        instance.birthday=validated_data['birthday']
        instance.celularphone=validated_data['celularphone']
        instance.comercialphone=validated_data['comercialphone']
        instance.residencialphone=validated_data['residencialphone']
        instance.country=validated_data['country']
        instance.state=validated_data['state']
        instance.city=validated_data['city']
        instance.street=validated_data['street']
        instance.number=validated_data['number']
        instance.complement=validated_data['complement']
        instance.gender=validated_data['gender']
        instance.current_price=validated_data['current_price']
        instance.psifollowbefore=validated_data['psifollowbefore']
        instance.medsbefore=validated_data['medsbefore']
        instance.followtime=validated_data['followtime']
        instance.medstime=validated_data['medstime']

        instance.save()

        return instance





class PatientsPlusSchedules(serializers.DocumentSerializer):

    next=SchedulesSerializer(many=True)

    class Meta:
        model = models.Patients
        fields="__all__"
        extra_kwargs={
            'payment_type': {'read_only': True},
            'psycologist_uuid': {'read_only': True},
            'patient_responsible': {'read_only': True},
            'created_at': {'read_only': True},
            'next_consult': {'read_only': True},
        }






class PatientResponsibleSerializer(serializers.EmbeddedDocumentSerializer):


    class Meta:
        model = models.PatientResponsible
        fields="__all__"


class PackageSerializer(serializers.DocumentSerializer):


    class Meta:
        model = models.Package
        fields="__all__"
        extra_kwargs={
            # 'patient': {'read_only': True},
            'psycologist_uuid': {'read_only': True},
        }
    def create(self, validated_data):
        pkg = models.Package.objects.create(**validated_data)
        
        pkg.save()

        return pkg

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.packagedone=validated_data['packagedone']
        instance.times=validated_data['times']
        instance.value=validated_data['value']

        instance.save()

        return instance



class ConsultsSerializer(serializers.DocumentSerializer):

    class Meta:
        model = models.Consults
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
            # 'patient': {'read_only': True},
            }

    def create(self, validated_data):
        consult = models.Consults.objects.create(**validated_data)
        
        consult.save()

        return consult

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.status=validated_data['status']
        instance.presence=validated_data['presence']
        instance.free=validated_data['free']
        instance.complaint=validated_data['complaint']
        instance.summary=validated_data['summary']
        instance.evolution=validated_data['evolution']
        instance.remainder=validated_data['remainder']
        instance.complaint_text=validated_data['complaint_text']
        instance.summary_text=validated_data['summary_text']
        instance.evolution_text=validated_data['complaint']
        instance.remainder_text=validated_data['complaint']
        

        instance.save()

        return instance


class SchedulesPlusPatientInfoSerializer(serializers.DocumentSerializer):

    patient_info = PatientsSerializer()

    class Meta:
        model = Schedules
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
        }


class DayScheduleObj(drf_serializer.Serializer):
    schedules=drf_serializer.ListField(child=SchedulesPlusPatientInfoSerializer())
    count=drf_serializer.IntegerField()
    day=drf_serializer.DateField()

class MonthScheduleSerializer(drf_serializer.Serializer):
    day=drf_serializer.DictField(child=DayScheduleObj())
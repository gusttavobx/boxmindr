import datetime
from django.db import models
from mongoengine import *
from django.db.models.signals import post_save


GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
    ('O', 'Outro')
)

class Package(Document):

    patient = ObjectIdField()
    psycologist_uuid = UUIDField(required=True)
    packagedone = BooleanField(default=False,blank=True)
    times = IntField(default=1,blank=True,null=True)
    value = FloatField(default=0.0,blank=True,null=True)


class Notes(EmbeddedDocument):
    notes = StringField(null=True)
    reminder = StringField(null=True)



class PatientResponsible(EmbeddedDocument):
    
    email = EmailField(max_length=128)
    first_name = StringField(max_length=40)
    last_name = StringField(max_length=40)
    birthday = DateTimeField()
    cpf = StringField(max_length=11)
    rg = StringField(max_length=10)
    celularphone = StringField(max_length=50)
    comercialphone = StringField(max_length=50)
    residencialphone = StringField(max_length=50)
    country = StringField(max_length=40 )
    state = StringField(max_length=40 )
    city = StringField(max_length=40 )
    street = StringField(max_length=64 )
    number = IntField()
    complement = StringField(max_length=128 )
    gender = StringField(max_length=1, choices=GENDER_CHOICES)



class Patients(Document):

    psycologist_uuid = UUIDField(required=True)
    email = EmailField(blank=True,null=True)
    first_name = StringField(max_length=64,blank=True,null=True)
    last_name = StringField(max_length=64,blank=True,null=True)
    cpf = StringField(max_length=11,blank=True,null=True)
    rg = StringField(max_length=10,blank=True,null=True)
    birthday = DateTimeField(blank=True,null=True)
    celularphone = StringField(max_length=50,blank=True,null=True) 
    comercialphone = StringField(max_length=50,blank=True,null=True)
    residencialphone = StringField(max_length=50,blank=True,null=True)
    country = StringField(max_length=40,blank=True,null=True)
    state = StringField(max_length=40,blank=True,null=True)
    city = StringField(max_length=40,blank=True,null=True)
    street = StringField(max_length=64,blank=True,null=True)
    number = IntField(blank=True,null=True)
    complement = StringField(max_length=128,blank=True,null=True)
    gender = StringField(max_length=1, choices=GENDER_CHOICES,blank=True,null=True)
    current_price = FloatField(blank=True,null=True)
    psifollowbefore = BooleanField(default=False,blank=True,null=True)
    medsbefore = BooleanField(default=False,blank=True,null=True)
    followtime = StringField(max_length=255, default= \
    "Este paciente já foi acompanhado por algum terapeuta?",blank=True,null=True)
    medstime = StringField(max_length=255,blank=True,null=True)
    created_at = DateTimeField(auto_now_add=True)
    next_consult = ListField(ObjectIdField())
    patient_responsible = EmbeddedDocumentField(PatientResponsible)

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"


class Consults(Document):
    
    psycologist_uuid = UUIDField(required=True)
    patient = ObjectIdField()
    status = BooleanField(default=False)
    presence = BooleanField(default=False)
    complaint = StringField()
    summary = StringField()
    evolution = StringField()
    remainder = StringField()
    complaint_text = StringField()
    summary_text = StringField()
    evolution_text = StringField()
    remainder_text = StringField()

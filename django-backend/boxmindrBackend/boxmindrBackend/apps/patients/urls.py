from . import views
from django.urls import include, path
from rest_framework_mongoengine import routers as merouters


router = merouters.DefaultRouter()

router.register('PatientsList', views.PatientsListViewSet, base_name='PatientsListViewSet')
router.register('PackagesList', views.PackagesListViewSet, base_name='PackagesListViewSet')
router.register('ConsultsList', views.ConsultsListViewSet, base_name='ConsultsListViewSet')


urlpatterns = [
    path('', include(router.urls)),
    path('nextconsultspatients/', views.NextConsultsPatients),
    path('patientsperrange/', views.PatientsPerRange),
    path('consultsperpatient/', views.ConsultsPerPatient),
    path('patientstoday/', views.PatientsToday),
]
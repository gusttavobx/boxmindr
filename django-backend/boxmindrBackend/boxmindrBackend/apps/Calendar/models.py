import datetime
from django.db import models
from mongoengine import *

SAZONALITY_CHOICES = (
    ('D', 'Day'),
    ('W', 'Week'),
    ('F', 'fortnight'),
    ('M', 'Month'),
    ('B', 'Bimester'),
    ('T', 'Trimester'),
    ('S', 'Simester'),
    ('Y', 'Year')
)
# Create your models here.
class Schedules(Document):

    consult_time = DateTimeField()
    duration = IntField()
    repetition = IntField()
    sazonality = StringField(max_length=1, choices=SAZONALITY_CHOICES)
    patient = ObjectIdField()
    psycologist_uuid = UUIDField(required=True)
    finalized = BooleanField()

def append_schedule_to_patient(sender, **kwargs):
    if kwargs['created']:
        # print(kwargs['document'].patient.next_consult)
        now = datetime.datetime.now()
        schedules_list=Schedules.objects(id__in=kwargs['document'].patient.next_consult)
        for schedule in schedules_list:
            if schedule.consult_time < now:
                kwargs['document'].patient.next_consult.remove(schedule.id)
        # TODO: Atualizr este metodo para usar __push
        kwargs['document'].patient.next_consult.append(kwargs['document'].id)
        kwargs['document'].patient.save()

post_save.connect(append_schedule_to_patient, sender=Schedules)



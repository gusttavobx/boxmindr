import datetime
from rest_framework_mongoengine import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework import permissions
from rest_framework.decorators import api_view
from mongoengine.queryset.visitor import Q
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from django.views.decorators.csrf import csrf_exempt

from Authentication.models import UserProfile
from Patients import models as patients_models
from Finances import models as finances_models

from Patients.serializers import SchedulesPlusPatientInfoSerializer, MonthScheduleSerializer, DayScheduleObj
from . import serializers
from . import models


from django.utils.decorators import method_decorator


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 10


UUID="1fa39c9a-c4f6-11e8-b8b2-bcee7b97176b"

# Create your views here.
class CalendarCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    # authentication_classes = (SessionAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.SchedulesSerializer
    pagination_class = StandardResultsSetPagination


    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=UUID)

    def get_queryset(self):

        psycologist = UserProfile.objects.get(
            email=self.request.user)
        queryset = models.Schedules.objects(psycologist_uuid=UUID)
        return queryset


@api_view(["GET"])
# @csrf_exempt
def MonthSchedule(request, *arg, **kwargs):
    if "first_day" and "last_day" in request.query_params:
        # print("first_day: ",first_day)
        # print("request.query_params['first_day']: ",request.query_params["first_day"])
        first_day=datetime.datetime.strptime(request.query_params["first_day"],"%Y-%m-%dT%H:%M:%S")
        last_day=datetime.datetime.strptime(request.query_params["last_day"],"%Y-%m-%dT%H:%M:%S")

        
        queryset=models.Schedules.objects(consult_time__gt=first_day,consult_time__lt=last_day, psycologist_uuid=request.user.uuid)

        print("first_day: ",first_day.__str__())
        if len(queryset) > 0:
            for item in queryset:
                item.patient_info=patients_models.Patients.objects(id=item.patient.id)
        
        qs=[]
        one_day=datetime.timedelta(days=1)
        day_end=first_day+one_day
        while first_day <= last_day:
            counter=0
            schedule_arr=[]
            for item in queryset:
                if item.consult_time >= first_day and item.consult_time <= day_end:
                    schedule_arr.append(
                        item
                    )
                    counter+=1
            day="{0}-{1}-{2}".format(first_day.day,first_day.month,first_day.year)
            qs.append({
                    "day":day,
                    "count":counter,
                    "schedules":schedule_arr
                    })
            
            first_day+=one_day
            day_end+=one_day

        print("qs: ",qs)
        serializer=DayScheduleObj(qs, many=True)

        return Response(serializer.data)

    
    return Response(status=status.HTTP_404_NOT_FOUND)



from . import models
from rest_framework_mongoengine import serializers
from rest_framework import serializers as drf_serializers
from Patients import serializers as p_serializer
from Finances import serializers as f_serializer

class SchedulesSerializer(serializers.DocumentSerializer):


    class Meta:
        model = models.Schedules
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
        }
        
    
    
    def create(self, validated_data):

        patient = models.Schedules.objects.create(**validated_data)
        return patient

    def update(self, instance, validated_data):
        
        instance.consult_time=validated_data['consult_time']
        instance.duration=validated_data['duration']
        instance.repetition=validated_data['repetition']
        instance.sazonality=validated_data['sazonality']
        instance.finalized=validated_data['finalized']

        instance.save()

        return instance

class FeedSerializer(drf_serializers.Serializer):

    psycologist_uuid=drf_serializers.UUIDField()
    consult=SchedulesSerializer()
    revenue=f_serializer.RevenuesSerializer()
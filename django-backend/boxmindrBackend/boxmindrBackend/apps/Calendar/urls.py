from . import views
from django.urls import include, path
from rest_framework_mongoengine import routers as merouters

from . import views

router = merouters.DefaultRouter()

router.register('calendarcrud', views.CalendarCRUDViewSet, base_name='CalendarCRUDViewSet')
router.register('feedcrud', views.FeedCRUDViewSet, base_name='FeedCRUDViewSet')


urlpatterns = [
    path('', include(router.urls)),
    path('monthschedule/', views.MonthSchedule, name="monthschedule"),
]
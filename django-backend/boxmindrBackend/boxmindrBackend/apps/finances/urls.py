from . import views
from django.urls import include, path
from rest_framework_mongoengine import routers as merouters


router = merouters.DefaultRouter()

router.register('expenses', views.ExpensesCRUDViewSet, base_name='ExpensesCRUDViewSet')
router.register('expensescategory', views.ExpensesCategoryCRUDViewSet, base_name='ExpensesCategoryCRUDViewSet')
router.register('revenues', views.RevenuesCRUDViewSet, base_name='RevenuesCRUDViewSet')
router.register('revenuescategory', views.RevenuesCategoryCRUDViewSet, base_name='RevenuesCategoryCRUDViewSet')


urlpatterns = [
    path('', include(router.urls)),
    path('revenuesperrange/',  views.RevenuesPerRange),
]
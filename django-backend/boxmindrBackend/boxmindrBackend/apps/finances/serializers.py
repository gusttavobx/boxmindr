import datetime
from rest_framework_mongoengine import serializers
from django.utils import timezone

from . import models


class ExpensesSerializer(serializers.DocumentSerializer):

    class Meta:
        model = models.Expenses
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
            '_cls': {'read_only': True},
            }

    def create(self, validated_data):
        expense = models.Expenses.objects.create(**validated_data)
        
        expense.save()

        return expense

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.price=validated_data['price']
        instance.status=validated_data['status']
        instance.presence=validated_data['presence']
        instance.notes.push()

        instance.save()

        return instance


class ExpensesCategorySerializer(serializers.DocumentSerializer):

    class Meta:
        model = models.ExpensesCategory
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
            }

    def create(self, validated_data):
        exp_category = models.ExpensesCategory.objects.create(**validated_data)
        
        exp_category.save()

        return exp_category

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.price=validated_data['price']
        instance.status=validated_data['status']
        instance.presence=validated_data['presence']
        instance.notes.push()

        instance.save()

        return instance

class RevenuesSerializer(serializers.DocumentSerializer):

    class Meta:
        model = models.Revenues
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
            'patient': {'read_only': True},
            }

    def create(self, validated_data):
        revenue = models.Revenues.objects.create(**validated_data)
        
        revenue.save()

        return revenue

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.value=validated_data['value']
        instance.maturity_date=validated_data['maturity_date']
        instance.status=validated_data['status']
        instance.repetition=validated_data['repetition']
        instance.category=validated_data['category']
        instance.comment=validated_data['comment']
        instance.payable_by=validated_data['payable_by']


        if validated_data["status"] == True and instance.payment_date is None:
            instance.payment_date=datetime.datetime.now()
        else:
            instance.payment_date=None

        instance.save()

        return instance



class RevenuesCategorySerializer(serializers.DocumentSerializer):

    class Meta:
        model = models.RevenuesCategory
        fields="__all__"
        extra_kwargs={
            'psycologist_uuid': {'read_only': True},
            'patient': {'read_only': True},
            }

    def create(self, validated_data):
        rev_category = models.RevenuesCategory.objects.create(**validated_data)
        
        rev_category.save()

        return rev_category

    def update(self, instance, validated_data):

        print("validated: ", validated_data)
        instance.price=validated_data['price']
        instance.status=validated_data['status']
        instance.presence=validated_data['presence']
        instance.notes.push()

        instance.save()

        return instance
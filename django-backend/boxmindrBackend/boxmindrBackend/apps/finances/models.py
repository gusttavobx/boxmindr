from django.db import models
from mongoengine import *
from Patients.models import Patients


# Create your models here.
class RevenuesCategory(Document):

    category = StringField(max_length=32, unique=True)
    psycologist_uuid = UUIDField()


class ExpensesCategory(Document):

  category = StringField(max_length=32, unique=True)
  psycologist_uuid = UUIDField()



class Revenues(Document):

    value = FloatField()
    maturity_date = DateTimeField(null=True, blank=True)
    payment_date = DateTimeField(null=True, blank=True)
    status = BooleanField(default=False)
    repetition = IntField(default=0)
    category = StringField(max_length=64, null=True, blank=True)
    comment = StringField(max_length=256, null=True, blank=True)
    psycologist_uuid = UUIDField()
    patient = ObjectIdField()
    payed_by = StringField()
    to_feed=BooleanField(default=False)


class Expenses(Document):

    value = FloatField()
    maturity_date = DateTimeField(null=True, blank=True)
    payment_date = DateTimeField(null=True, blank=True)
    status = BooleanField(default=False)
    repetition = IntField(null=True, blank=True)
    category = ReferenceField(ExpensesCategory)
    comment = StringField(max_length=256, null=True, blank=True)
    psycologist_uuid = UUIDField()
    patient = ObjectIdField()
    patient_name = StringField()

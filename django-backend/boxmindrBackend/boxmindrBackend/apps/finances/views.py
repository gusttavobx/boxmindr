import datetime
from rest_framework_mongoengine import viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.utils import timezone
from mongoengine.queryset.visitor import Q

from . import models
from . import serializers
from Authentication.models import UserProfile
from Calendar.models import Schedules
from Patients.models import Patients


UUID="1fa39c9a-c4f6-11e8-b8b2-bcee7b97176b"

# Create your views here.
class ExpensesCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.ExpensesSerializer

    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=psycologist.uuid)

    def get_queryset(self):

        psycologist = UserProfile.objects.get(
            email=self.request.user)
        queryset = models.Expenses.objects(psycologist_uuid=psycologist.uuid)
        return queryset



class ExpensesCategoryCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.ExpensesCategorySerializer

    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=psycologist.uuid)

    def get_queryset(self):

        psycologist = UserProfile.objects.get(
            email=self.request.user)
        queryset = models.ExpensesCategory.objects(psycologist_uuid=psycologist.uuid)
        return queryset




class RevenuesCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.RevenuesSerializer

    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=psycologist.uuid)

    def get_queryset(self):

        queryset = models.Revenues.objects(psycologist_uuid=UUID)
        return queryset



class RevenuesCategoryCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.RevenuesCategorySerializer

    def perform_create(self, serializer):
        
        psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=psycologist.uuid)

    def get_queryset(self):

        psycologist = UserProfile.objects.get(
            email=self.request.user)
        queryset = models.RevenuesCategory.objects(psycologist_uuid=psycologist.uuid)
        return queryset



@api_view(["GET"])
def RevenuesPerRange(request, *args, **kwargs):

    if 'range' in request.query_params:
        if request.query_params['range'] == "today":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            day_end=datetime.datetime(now.year,now.month,now.day,23,59,59)
            queryset=models.Revenues.objects(Q(maturity_date__gt=today) &
                                Q(maturity_date__lt=day_end))
            
            
            serializer=serializers.RevenuesSerializer(queryset, many=True)
            return Response(serializer.data)

        if request.query_params['range'] == "week":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            difference1 = datetime.timedelta(weeks=1)
            week_end=today+difference1
            queryset=models.Revenues.objects(Q(maturity_date__gt=today) &
                                Q(maturity_date__lt=week_end))
            

            
            serializer=serializers.RevenuesSerializer(queryset, many=True)
            return Response(serializer.data)

        if request.query_params['range'] == "month":
            now=datetime.datetime.now()
            today=datetime.datetime(now.year,now.month,now.day,0,0,1)
            difference1 = datetime.timedelta(weeks=4)
            month_end=today+difference1
            queryset=models.Revenues.objects(Q(maturity_date__gt=today) &
                                Q(maturity_date__lt=month_end))
            
            
            serializer=serializers.RevenuesSerializer(queryset, many=True)
            return Response(serializer.data)
    else: 
        return Response(None)
from django.shortcuts import render

# Create your views here.
@api_view(['GET'])
def ConsultsToFeedView(request, *args, **kwargs):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    last_login=datetime.datetime(2018,10,9, 10, 0, 0)

    scheduled_consults = models.Schedules.objects(
        consult_time__gt=last_login,
        psycologist_uuid=UUID
    )
    queryset = []
    for item in scheduled_consults:
        if item.finalized == False:
            duration=datetime.timedelta(minutes=item.duration)
            if item.consult_time+duration < datetime.datetime.now():
                item.finalized = True
                item.save()
                # request.user.uuid
                feed=models.Feed(
                    psycologist_uuid=UUID,
                    consult=item
                )
                feed.save()
                queryset.append(feed)
    
    serializer=serializers.FeedSerializer(queryset, many=True)
    return Response(serializer.data)


class FeedCRUDViewSet(viewsets.ModelViewSet):
    """  The actions provided by the ModelViewSet class are 
    .list(), .retrieve(), .create(), .update(), .partial_update(), 
    and .destroy().  """

    # authentication_classes = (SessionAuthentication,)
    # permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.FeedSerializer

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(FeedCRUDViewSet, self).dispatch(request, *args, **kwargs)

    def perform_create(self, serializer):
        
        # psycologist = UserProfile.objects.get(email=self.request.user)
        serializer.save(psycologist_uuid=UUID)

    def get_queryset(self):

        # psycologist = UserProfile.objects.get(
            # email=self.request.user)
        queryset = models.Feed.objects(psycologist_uuid=UUID)
        return queryset

@api_view(['GET'])
def RevenuesToFeedView(request, *args, **kwargs):
    last_login=datetime.datetime(2018,10,9, 10, 0, 0)
    now=datetime.datetime.now()
    today=datetime.datetime(now.year,now.month,now.day, 0, 0, 0)
    one_day=datetime.timedelta(days=1)
    tomorrow=today+one_day

    revenues=finances_models.Revenues.objects(psycologist_uuid=UUID)

    queryset=[]
    for item in revenues:
        if item.to_feed == False and item.maturity_date <= tomorrow:
            item.to_feed = True
            item.save()
            # request.user.uuid
            feed=models.Feed(
                psycologist_uuid=UUID,
                revenue=item
            )
            feed.save()
            queryset.append(feed)

    serializer=serializers.FeedSerializer(queryset, many=True)
    return Response(serializer.data)
from django.db import models
from boxmindrBackend.apps.Calendar import Schedules
from boxmindrBackend.apps.finances import Revenues

# Create your models here.
class Feed(Document):
    
    psycologist_uuid = UUIDField(required=True)
    consult=ReferenceField(Schedules, reverse_delete_rule=CASCADE, null=True)
    revenue=ReferenceField(Revenues, reverse_delete_rule=CASCADE, null=True)
from . import views
from django.urls import include, path

from . import views


urlpatterns = [
    path('consultstofeed/', views.ConsultsToFeedView),
    path('revenuestofeed/', views.RevenuesToFeedView),
]